# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'
    requisition_history_sale = fields.Integer("Requisition History Sale")

    @staticmethod
    def default_requisition_history_sale():
        return 90
